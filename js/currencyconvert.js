/* JavaScript to;
 * 
 * extract amount for conversion from window.location GET variable
 * retreive data from openexchangerates.org via PHP
 * if that fails
 * retreive data from openexchangerates.org via JavaScript
 * if that fails
 * use the default cached exchange rates
 * convert from USD-base to AUD-base
 * (licence for supplied APP-ID won't permit AUD-base from openexchangerates.org)
 * (licence for supplied APP-ID won't permit currency conversion at openexchangerates.org)
 * prepare specified data for display
 * write HTML table
 * 
 * - author: Robin Goodfellow
 */

$(document).ready(function() {
  parseAmount();
});

var amount;
var exchange;

function parseAmount() {
  // expect any number of GET variables
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
    vars[key] = value;
  });
  if (vars['amount'] && $.isNumeric(vars['amount'])) {
    // only continue if we have a numeric amount
    amount = vars['amount'];
    $('#amount').html(amount);
    retrieveData();
  } else {
    $('#amount').html(' invalid amount ');
  }
}

function retrieveData() {
  // get rates via PHP script
  var getAmount = $.ajax({
    url: 'inc/currencyconverter.php',
    dataType: 'json'
  }).done(function(data) {
    if (data.status == 'success') {
      // we have some new data
      exchange = data.data;
    } else {
      // try fetching via JavaScript
      retryFetching(data.data)
    }
    // some data is now ready for display
    parseData();
  });
}

function retryFetching(appID) {
  // fetch data from openexchangerates.org via JavaScript
  $.get('https://openexchangerates.org/api/latest.json', {app_id: appID}, function(newData) {
    if (newData.timestamp && newData.rates) {
    // use the retrieved data
    exchange = newData;
    } else {
    // use the cached data from file
    exchange = exchangerates;
    }
  });
}

function calculateConversion(cur) {
  // calculate the rate and conversion for a currency
  // base is USA, convert to AUD first
  var aud = 1;
  // don't divide by zero
  if (exchange.rates.AUD != 0) {
    aud = 1 / exchange.rates.AUD;
  }
  // find conversion rate
  var rate = exchange.rates[cur] * aud;
  // calculate converted value
  var conv = amount * rate;
  // return
  return {'rate':rate.toFixed(4),'conv':conv.toFixed(2)};
}

function parseTimestamp() {
  // needs to be * 1000 because JavaScript uses mil1iseconds for Date Object
  // and the timestamp returned from openexchange is in seconds
  var dateObj = new Date(exchange.timestamp * 1000);
  return dateObj.toISOString().substring(0,10) + ' ' + dateObj.toISOString().substring(11,19);
}

function parseData() {
  // prepare data for writing to HTML
  var printObj = {
    'datetime':parseTimestamp(),
    'data':[
      {
        'currency':'USD',
        'rate':calculateConversion('USD').rate,
        'converted':calculateConversion('USD').conv
      },
      {
        'currency':'JPY',
        'rate':calculateConversion('JPY').rate,
        'converted':calculateConversion('JPY').conv
      },
      {
        'currency':'GBP',
        'rate':calculateConversion('GBP').rate,
        'converted':calculateConversion('GBP').conv
      },
      {
        'currency':'NZD',
        'rate':calculateConversion('NZD').rate,
        'converted':calculateConversion('NZD').conv
      }
    ]
  };
  writeTable(printObj);
}

function writeTable(printObj) {
  // write HTML
  var htmlString = '<table>';
  htmlString += '<tr><th>Currency</th>';
  htmlString += '<th>Exchange Rate</th>';
  htmlString += '<th>Converted Value</th>';
  htmlString += '<th>Retrieved At</th></tr>';
  for (var i = 0; i < printObj.data.length; i++) {
    if (printObj.data) {
      htmlString += '<tr><td>' + printObj.data[i].currency + '</td>';
      htmlString += '<td>' + printObj.data[i].rate + '</td>';
      htmlString += '<td>' + printObj.data[i].converted + '</td>';
      htmlString += '<td>' + printObj.datetime + '</td></tr>';
    }
  }
  htmlString += '</table>'
  $('#converted').html(htmlString);
}
