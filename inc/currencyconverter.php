<?php
/* PHP script to collect exchange rates
 * 
 * @author Robin Goodfellow
 */

$exchangeRates = new ExchangeRates;
print(json_encode($exchangeRates->getRates()));

class ExchangeRates {
  private $url;
  private $appID;
  
  function __construct() {
    $this->url = 'https://openexchangerates.org/api/latest.json?app_id=';
    $this->appID = '26c102aa26c84e20acd4673d624bc9b8';
  }
  
  function getRates() {
    // try curl first
    if (function_exists(curl_init)) {
      return $this->useCurl();
    }
    // try fopen
    $fdata = $this->useFopen();
    if ($fdata->status !== 'fail') {
      return $fdata;
    }
    // fail on both - return appID to JavaScript
    return array(
      'status' => 'fail',
      'data' => $this->appID
    );
  }
  
  function useCurl() {
    // Open CURL session:
    $curl_handle = curl_init($this->url);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
    
    // Get the data:
    $contents = curl_exec($curl_handle);
    curl_close($curl_handle);
    
    // Return decoded JSON response:
    return array(
      'status' => 'success',
      'data' => json_decode($contents)
    );
  }
  
  function useFopen() {
    $fh = fopen($this->url.$this->appID,'r');
    if ($fh === false) {
      return array(
        'status' => 'fail',
        'data' => $this->appID
      );
    }
    $contents = stream_get_contents($fh);
    fclose($fh);
    return array(
      'status' => 'success',
      'data' => json_decode($contents)
    );
  }
}
