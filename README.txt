Currency Converter for use with openexchangerates.org
=====================================================

Usage:
The instructions included the line: 'The price of the item will be passed to the
PHP script as a GET variable in the URL, with the name of “amount”.' While I
noticed this as part of the instructions for the openexchangerates API, I took
it to mean that this is to be applied to the path when accessing this project.
The PHP script is accessed with the path index.php?amount=23.45 (as per the
supplied example).

First investigations:
When reading the API documentation and testing https://openexchangerates.org/ I
noticed that the license for the App ID supplied did not allow for currency
conversion on the site, nor retrieving the latest exchange rates with an AUD
base. I realised that I would need to retrieve the latest exchange rates with
the default USD base, then convert to AUD.

API assumptions:
The API documentation provided example code and recommendatioons in a number of
coding languages. Despite the provided instruction "assume the API will never
fail or return a malformed response", my use of the example code with curl_init
resulted in failure to fetch, as the installation of PHP 7.1 on my desktop
computer didn't include curl, and installing curl for PHP 7.1 still didn't
manage to get it working. Switching to my laptop with PHP 5.6 installed didn't
result in improvement. Using fopen as a fallback in case the suggested curl
fails, resulted in success. Another option from openexchangerates was to use
JavaScript; as this is the front-end language, I use this as a fallback if both
PHP options fail. If all those options fail, I have a "cached" copy of the data
from openexchangerates.
