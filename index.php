<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Currency Converter</title>
    <link rel="stylesheet" href="css/currencyconvert.css">
  </head>
  <body>
    <div>
      <h1>Converting $<span id="amount"></span> AUD</h1>
      <div id="converted"></div>
    </div>
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/exchangerates.js"></script>
    <script type="text/javascript" src="js/currencyconvert.js"></script>
  </body>
</html>